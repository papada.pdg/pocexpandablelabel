//
//  ViewController.swift
//  POCExpandableLabel
//
//  Created by Papada Preedagorn on 25/12/2562 BE.
//  Copyright © 2562 Papada Preedagorn. All rights reserved.
//

import UIKit

struct GMBReviewSectionViewModel {
  let title: String
  let subTitle: String
  let status: GMBReviewStatus
  let iconFail: String
  let titleFail: String
  let iconEmpty: String
  let titleEmpty: String
  let textReadMore: String
  let reviewers: [Reviewer]
}

struct Reviewer {
  let image: String
  let name: String
  let description: String
  let rating: Int
}

enum GMBReviewStatus {
  case success
  case fail
  case empty
}

class ViewController: UIViewController {
  
  @IBOutlet weak var vStack: UIStackView!
  @IBOutlet weak var titleGMBReview: UILabel!
  @IBOutlet weak var subtitleGMBReview: UILabel!
  
  let items = GMBReviewSectionViewModel(title: "รีวิวร้านค้าจากผู้ใช้ Google", subTitle: "ข้อมูลรีวิว 5 รีวิวล่าสุด", status: .success, iconFail: "", titleFail: "ไม่สามารถทำรายการได้ในขณะนี้", iconEmpty: "", titleEmpty: "ไม่มีรีวิวร้านค้าจากผู้ใช้", textReadMore: "อ่านทั้งหมด", reviewers: [
    Reviewer(image: "", name: "Ratchaneeporn Nornma", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in sem arcu. Morbi nec diam non libero lacinia suscipit vel nec turpis. In ac massa ac mi consequat tempus non vitae neque. Mauris eu tempor elit. Integer convallis justo et urna posuere, ac sodales ipsum elementum. Etiam efficitur pretium rutrum. Proin id tempor dui, sit amet dignissim mauris.", rating: 4),
    Reviewer(image: "", name: "Suda Jaidee", description: "LoremipsumdolorsitametconsecteturadipiscingelitSuspendisseinsemarcuMorbinecdiamnonliberolaciniasuscipitvelnecturpisInacmassaacmiconsequattempusnonvitaenequeMauriseutemporelitIntegerconvallisjustoeturnaposuereLoremipsumdolorsitametconsecteturadipiscingelitSuspendisseinsemarcuMorbinecdiamnonliberolaciniasuscipitvelnecturpis", rating: 4),
    Reviewer(image: "", name: "Suda Jaidee", description: "สวัสดี1สวัสดี2สวัสดี3สวัสดี4สวัสดี5สวัสดี6สวัสดี7สวัสดี7สวัสดี8สวัสดี9สวัสดี10สวัสดี11สวัสดี12สวัสดี13สวัสดี14สวัสดี15สวัสดี16สวัสดี17สวัสดี18สวัสดี19สวัสดี20สวัสดี21สวัสดี22สวัสดี23สวัสดี24สวัสดี25สวัสดี26สวัสดี27สวัสดี28สวัสดี29สวัสดี30สวัสดี31สวัสดี32สวัสดี33สวัสดี34สวัสดี35สวัสดี36สวัสดี37สวัสดี38สวัสดี39สวัสดี40", rating: 2),
    Reviewer(image: "", name: "Suda Jaidee", description: "1\n2\n3\n4\n5\n6", rating: 2),
    Reviewer(image: "", name: "Suda Jaidee", description: "asdsadasdasd", rating: 2),
    Reviewer(image: "", name: "Suda Jaidee", description: "สวัสดี1\nสวัสดี2\nสวัสดี1\nสวัสดี2\nสวัสดี1\nสวัสดี2", rating: 2),
    Reviewer(image: "", name: "Suda Jaidee", description: "1234759872398471897419838913781047049128490184901809481098093812903810938901283902183901890184901704170491903810231048712083710391390712038713807180371030128730814739819038123984718974198389137810470491284901849018094810980938129038109389012839021839018901849017041704919038102310487120837103913907120387138071803710301287308147398190381", rating: 2),
    Reviewer(image: "", name: "Suda Jaidee", description: "diasgduasbfiuabudadbaiyfbaihcbihabciuabdsiuabdiubauidbasidbiabdiuabduandnafiabfbayduadiaudhaosdbaidiaudobadiahvdiabduabdiaubdiabdiuabduabdibauidbaiudbuiabduabduiabduabdiuabdiuabdiuabiuabudadbaiyfbaihcbihabciuabdsiuabdiubauidbasidbiabdiuabduandnafiabfbayduadiaudhaosdbaidiaudobadiahvdiabduabdiaubdiabdiuabduabdibauidbaiudbuiabduabduiabduabdiuabdiuabdiuab", rating: 2),
    Reviewer(image: "", name: "Suda Jaidee", description: "DFOJBNFDJABDKJADBNJAKDBHKDBASKDBAFBU3HQODNOBDUISABFKJANCJNBAKJCNOIAJXIAJDPADOJDPAJFOIAIDNADBNAIDBFIAUSNDIONASONAIONDIOPANDPIANDPIANDPIONAPODAPDADAPDNADNALKNKLANFLKANLNXNXNDPINALKANLNXNXNDPINA", rating: 2),
 Reviewer(image: "", name: "Suda Jaidee", description: "DFOJBNFDJABDKJADBNJAKDBHKDBASKDBAFBU3HQODNOBDUISABFKJANCJNBAKJCNOIAJXIAJDPADOJDPAJFOIAIDNADBNAIDBFIAUSNDIONASONAIONDIOPANDPIANDPIANDPIONAPODAPDADAPDNADNALKNKLADSA", rating: 2),
 Reviewer(image: "", name: "Reviewer name", description: "Everything is bad", rating: 0),
  ])
  
  override func viewDidLoad() {
    super.viewDidLoad()
    vStack.translatesAutoresizingMaskIntoConstraints = false
    setupUIGMBReviewsView()
  }
  
  func setupUIGMBReviewsView(){
    var image : UIImage
    switch items.status {
    case .success:
      titleGMBReview.text = items.title
      subtitleGMBReview.text = items.subTitle
      updateUICellGMBReviewView()
    case .fail:
      subtitleGMBReview.isHidden = true
      let errorView = ErrorGMBReviewSectionView.loadViewFromNib()
      image = UIImage(named: "Star") ?? UIImage()
      errorView.setupUI(image: image, descriptionError: items.titleFail)
      vStack.addArrangedSubview(errorView)
    default:
      subtitleGMBReview.isHidden = true
      let emptyView = ErrorGMBReviewSectionView.loadViewFromNib()
      image = UIImage(named: "Star") ?? UIImage()
      emptyView.setupUI(image: image, descriptionError: items.titleEmpty)
      vStack.addArrangedSubview(emptyView)
    }
  }
  
  func updateUICellGMBReviewView() {
    let reviewers = items.reviewers
    for item in 0...reviewers.count - 1 {
      let reviewerView = GMBReviewSectionView().loadViewFromNib()
      let imageSelected = UIImage(named:"Star") ?? UIImage()
      let imageUnselected = UIImage(named: "unStar") ?? UIImage()
      reviewerView.updateUI(item: reviewers[item], textReadMore: items.textReadMore, starSelectedImage: imageSelected, starDeselectImage: imageUnselected)
      vStack.addArrangedSubview(reviewerView)
      if item == reviewers.count - 1 {
        reviewerView.hideSeparatorLine()
      }
    }
  }
}
