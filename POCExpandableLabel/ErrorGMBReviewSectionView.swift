//
//  ErrorGMBReviewSectionView.swift
//  POCExpandableLabel
//
//  Created by Papada Preedagorn on 9/1/2563 BE.
//  Copyright © 2563 Papada Preedagorn. All rights reserved.
//

import UIKit

class ErrorGMBReviewSectionView: UIView {
  
  @IBOutlet weak var imageError: UIImageView!
  @IBOutlet weak var descriptionErrorText: UILabel!
  
  class func loadViewFromNib() -> ErrorGMBReviewSectionView {
    let bundle = Bundle(for: ErrorGMBReviewSectionView.self)
    guard let errorView = UINib(nibName: String(describing: self), bundle: bundle).instantiate(withOwner: nil, options: nil).first as? ErrorGMBReviewSectionView else {
      return ErrorGMBReviewSectionView()
    }
    return errorView
  }
  
  func setupUI(image: UIImage, descriptionError: String) {
    imageError.image = image
    descriptionErrorText.text = descriptionError
  }
}
