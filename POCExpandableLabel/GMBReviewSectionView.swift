//
//  commentsGoogleView.swift
//  POCExpandableLabel
//
//  Created by Papada Preedagorn on 26/12/2562 BE.
//  Copyright © 2562 Papada Preedagorn. All rights reserved.
//

import UIKit

class GMBReviewSectionView: UIView {
    @IBOutlet private weak var commentText: UILabel!
    @IBOutlet private weak var nameUserReviewInGoogle: UILabel!
    @IBOutlet private weak var iconUserReview: UIImageView!
    @IBOutlet private weak var separatorLine: UIView!
    @IBOutlet private weak var readmorebutton: UILabel!
    @IBOutlet private weak var commentsGoogleView: UIStackView!
    
    @IBOutlet private weak var starStackView: UIStackView!
    @IBOutlet private weak var star1: UIImageView!
    @IBOutlet private weak var star2: UIImageView!
    @IBOutlet private weak var star3: UIImageView!
    @IBOutlet private weak var star4: UIImageView!
    @IBOutlet private weak var star5: UIImageView!
    
    private var starArray: [UIImageView] = []
    
    private func setStar(starSelectedImage: UIImage, starDeselectImage: UIImage, rating: Int) {
      let indexStar: Int = rating - 1
      initialStar(starDeselectImage: starDeselectImage)
      guard rating > 0 else {
        return
      }
      setImageStar(starDeselectImage: starSelectedImage, indexStar: indexStar)
    }
    
    private func initialStar(starDeselectImage: UIImage?) {
      starArray = [star1, star2, star3, star4, star5]
      let maxStarArrayCount = starArray.count - 1
      for star in 0...maxStarArrayCount {
        starArray[star].image = starDeselectImage
      }
    }
    
    private func setImageStar(starDeselectImage: UIImage, indexStar: Int) {
      for star in 0...indexStar {
        starArray[star].image = starDeselectImage
      }
    }
    
    public func updateUI(item: Reviewer, textReadMore: String, starSelectedImage: UIImage, starDeselectImage: UIImage) {
      commentText.text = item.description
      nameUserReviewInGoogle.text = item.name
//      iconUserReview.kf.setImage(with: URL(assetsURL: item.image))
      let starSelectedImage = starSelectedImage
      let starDeselectImage = starDeselectImage
      setStar(starSelectedImage: starSelectedImage, starDeselectImage: starDeselectImage, rating: item.rating)
      self.readmorebutton.isHidden = !self.commentText.maxNumberOfLines
      let tap = UITapGestureRecognizer(target: self, action: #selector(GMBReviewSectionView.tapFunction))
      commentsGoogleView.addGestureRecognizer(tap)
    }
    
    @objc
    private func tapFunction(sender: UITapGestureRecognizer) {
      self.readmorebutton.isHidden = true
      UIView.transition(with: commentText, duration: 0.5, options: .transitionCrossDissolve, animations: { [weak self] in
        self?.commentText.numberOfLines = 0
      })
    }
    
     public func loadViewFromNib() -> GMBReviewSectionView {
      let bundle = Bundle(for: GMBReviewSectionView.self)
       guard let view = UINib(nibName: "GMBReviewSectionView", bundle: bundle).instantiate(withOwner: nil, options: nil).first as? GMBReviewSectionView else {
         return GMBReviewSectionView()
       }
       return view
     }
    
    public func hideSeparatorLine() {
      separatorLine.isHidden = true
    }
    
  }

  extension UILabel{
    var isTruncated: Bool {
      guard let actualText = self.text, let safeFont = font else { return false }
      if actualText.count == 0 { return false }
      let labelWidth: CGFloat = self.frame.size.width
      let maximumLabelSize = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
      let labelHeight = sizeThatFits(maximumLabelSize).height
      let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
      let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: safeFont]
      let attributedText = NSAttributedString(string: actualText, attributes: attributes as? [NSAttributedString.Key : Any])
      let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
      return labelHeight < boundingRect.size.height
    }
    
    var maxNumberOfLines: Bool {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
      guard let text = self.text else { return false }
      let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font ?? UIFont()], context: nil).height
        let lineHeight = font.lineHeight
      return numberOfLines < Int(ceil(textHeight / lineHeight))
    }

}

